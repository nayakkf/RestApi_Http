package com.qa.tests;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qa.base.TestBase;
import com.qa.client.RestClient;
import com.qa.config.DataReader;

public class PostApiTest extends TestBase {
	TestBase base;
	String serviceUrl;
	String apiUrl;
	String actualUrl;
	RestClient restClient ;
	CloseableHttpResponse httpResponce;

	@BeforeMethod
	public void setUp()
	{
		base = new TestBase();
		serviceUrl= prop.getProperty("URL");
		apiUrl=prop.getProperty("serviceURL");
		actualUrl=serviceUrl+apiUrl;

	}

	@Test
	public void pohttpClientstApiTest() throws JsonGenerationException, JsonMappingException, IOException
	{

		restClient= new RestClient();
		HashMap<String, String> hashMap= new HashMap<String, String>();
		hashMap.put("content-type", "application/json");

		//Jackson API:
		ObjectMapper mapper= new ObjectMapper();
		DataReader dataReader= new DataReader("morpheus","leader","","");//expected user 

		//Object to Json  in File-Marshelling
		mapper.writeValue(new File("E:\\ApiTesting\\JSONPath\\user.json"), dataReader);

		//object to Json in string--marshelling
		String userJsonString=mapper.writeValueAsString(dataReader);
		System.out.println("User JsonString is:->"+userJsonString);

		//Hit the API
		httpResponce=restClient.post(actualUrl, userJsonString, hashMap);


		//Validatig part::::::::::::::::::
		int statusCode=httpResponce.getStatusLine().getStatusCode();
		Assert.assertEquals(base.RESPONCE_STATUS_CODE_201, statusCode);

		//Json String
		String responce=EntityUtils.toString(httpResponce.getEntity(),"UTF-8");		
		JSONObject responceJson= new JSONObject(responce);
		System.out.println("Responce from API is:->"+responceJson);

		//json to java object-Unmarshelling
		DataReader userResObj=mapper.readValue(responce, DataReader.class);//actual user obj
		System.out.println(userResObj);

		Assert.assertTrue(dataReader.getName().equals(userResObj.getName()));
		Assert.assertTrue(dataReader.getJob().equals(userResObj.getJob()));

		//Printing the message
		System.out.println(userResObj.getId());
		System.out.println(userResObj.getCreatedAt());


	}

}
