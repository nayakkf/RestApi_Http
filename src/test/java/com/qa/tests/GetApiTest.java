package com.qa.tests;

import java.io.IOException;
import java.util.HashMap;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.client.RestClient;
import com.qa.utility.TestUtility;

public class GetApiTest extends TestBase {


	TestBase testBase;
	String serviceUrl;
	String actualUrl;
	String apiUrl;
	RestClient restClient;
	CloseableHttpResponse closeableHttpResponce;

	@BeforeMethod
	public void setUp()
	{
		testBase = new TestBase();
		serviceUrl= prop.getProperty("URL");
		apiUrl=prop.getProperty("serviceURL");
		actualUrl=serviceUrl+apiUrl;


	}

	@Test
	public void getTest() throws ClientProtocolException, IOException
	{
		restClient= new RestClient();
	    closeableHttpResponce= restClient.get(actualUrl);

		//Status Code
		int statusCode=closeableHttpResponce.getStatusLine().getStatusCode();
		System.out.println("Status code is:-> "+statusCode);
		
		Assert.assertEquals(statusCode,RESPONCE_STATUS_CODE_200,"Status code is not 200");

		//Fetch all the details in String format:Json String
		String responce=EntityUtils.toString(closeableHttpResponce.getEntity(),"UTF-8");

		//Creating in JsonObject
		JSONObject jsonObject=new JSONObject(responce);
		System.out.println("Responce Json from API:-> "+jsonObject);

		//Fetching the value from JsonObject
		String per_pageValue=TestUtility.getValueByJsonPath(jsonObject,"/per_page");
		System.out.println("Value of Per_page is :->"+per_pageValue);
		Assert.assertEquals(Integer.parseInt(per_pageValue),6);
		
		String total=TestUtility.getValueByJsonPath(jsonObject,"/total");
		System.out.println("Value of total is :->"+total);
		Assert.assertEquals(Integer.parseInt(total),12);

		//Fetching the value from JsonArray	
		String lastName=TestUtility.getValueByJsonPath(jsonObject,"/data[0]/last_name");
		String id=TestUtility.getValueByJsonPath(jsonObject,"/data[0]/id");
		String avatar=TestUtility.getValueByJsonPath(jsonObject,"/data[0]/avatar");
		String firstName=TestUtility.getValueByJsonPath(jsonObject,"/data[0]/first_name");

		System.out.println(lastName);
		System.out.println(id);
		System.out.println(avatar);
		System.out.println(firstName);


		//Fetching all the information from header in Array format
		Header[] headerArray=closeableHttpResponce.getAllHeaders();

		//Created a HashMap for looping into Key value pair
		HashMap<String, String> allHeaders= new HashMap<String, String>();
		for(Header header:headerArray)
		{
			allHeaders.put(header.getName(), header.getValue());
		}
		System.out.println("Headers are :-> "+allHeaders);


	}

}
