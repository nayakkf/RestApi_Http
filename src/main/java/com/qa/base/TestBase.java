package com.qa.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class TestBase {

	
	public Properties prop;
	public int RESPONCE_STATUS_CODE_200=200;
	public int RESPONCE_STATUS_CODE_400=400;
	public int RESPONCE_STATUS_CODE_500=500;
	public int RESPONCE_STATUS_CODE_201=201;
	
	public TestBase()
	{
		//Basically It is used for loading all the Url And basic details required for Execution that is placed.
		try{
		    prop= new Properties();
			FileInputStream inputStream= new FileInputStream(System.getProperty("user.dir")+"/src/main/java/com/qa/config/config.properties");
			prop.load(inputStream);			
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
