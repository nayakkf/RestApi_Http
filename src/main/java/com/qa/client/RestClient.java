package com.qa.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.testng.Assert;

import com.qa.utility.TestUtility;

public class RestClient {
	
	//1 Get Method
	public CloseableHttpResponse get(String url) throws ClientProtocolException, IOException
	{
		CloseableHttpClient httpClient=	HttpClients.createDefault();
		HttpGet httpget=new HttpGet(url);
		CloseableHttpResponse closeableHttpResponce= httpClient.execute(httpget);		
		return 	closeableHttpResponce;		
	}
	
	//1.GET Method without Headers(Using Url and headers) using CloseableHttpClient and Httpget class
	public CloseableHttpResponse getConnection(String url,HashMap<String, String>headerMap) throws ClientProtocolException, IOException
	{
		//Create one client connection 
		CloseableHttpClient httpClient=	HttpClients.createDefault();

		//Creating one Get url connection for validating the responce
		HttpGet httpget=new HttpGet(url);

		//For looping around the all headers	
		for(Map.Entry<String,String>entry:headerMap.entrySet())
		{
			//Add Header is a method for adding all methods
			httpget.addHeader(entry.getKey(), entry.getValue());
		}

		//Hit the API(GET url)
		CloseableHttpResponse httpResponce= httpClient.execute(httpget);

		return httpResponce;

	}

	//Post Method
	public CloseableHttpResponse post(String url,String entityString, HashMap<String,String>headerMap) throws ClientProtocolException, IOException
	{
		//Create one client connection
		CloseableHttpClient httpClient=	HttpClients.createDefault();	
		HttpPost httpPost= new HttpPost(url);//Posting the information	
		
		httpPost.setEntity(new StringEntity(entityString));//Defining the payload	
		//For Headers
		for(Map.Entry<String,String>entry:headerMap.entrySet())
		{
			httpPost.addHeader(entry.getKey(), entry.getValue());
		}
		CloseableHttpResponse closeablehttpResponce= httpClient.execute(httpPost);	
		System.out.println("Pass");
		return closeablehttpResponce;	
		
	}

	public CloseableHttpResponse put(String url,String entityString, HashMap<String,String>headerMap) throws ClientProtocolException, IOException
	{
		CloseableHttpClient httpClient=	HttpClients.createDefault();
		HttpPut httpPut=new HttpPut(url);
		
		httpPut.setEntity(new StringEntity(entityString));//Defining the payload

		//For Headers
		for(Map.Entry<String,String>entry:headerMap.entrySet())
		{
			httpPut.addHeader(entry.getKey(), entry.getValue());
		}
		CloseableHttpResponse closeablehttpResponce= httpClient.execute(httpPut);			
		return closeablehttpResponce;	
	}

}
