package com.qa.config;

//pojo->plain old java object
public class DataReader {
	String name;
	String job;
	String id;
	String createdAt;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public DataReader()
	{
		
	}
	public DataReader(String name,String job,String id,String createdAt)
	{
		this.name=name;
		this.job=job;
		this.id=id;
		this.createdAt=createdAt;
			
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	
	//Getters and setters
	
	

}
